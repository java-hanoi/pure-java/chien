public class Chien {
	
	String race;
	String abboiement;
	String couleur;
	
	public Chien () {
		race = "Inconnue";
		abboiement = "Inconnu";
		couleur = "Inconnue";
	}
		
	public Chien (String pRace, String pAbboiement, String pCouleur)
	{
		race = pRace;
		abboiement = pAbboiement;
		couleur = pCouleur;
	}
	
	public static void main (String[] args) {
		Chien Médor = new Chien ("Labrador", "Ouaf", "Beige");
		Chien Toutou = new Chien ("Rottweiler", "Wouf", "Noir");
		System.out.println(Médor.race);
		System.out.println(Toutou.race);
	}
}