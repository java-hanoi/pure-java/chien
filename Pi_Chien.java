public class Chien {
	public String noise;
	public String color;
	
	public Chien() {
		//valeurs par défauts 
		noise = "woof";
		color = "noir";
	}
	
	public Chien(String noise, String color) {
		this.noise = noise;
		this.color = color;
	}
	
	public static void main(String[] args) {
		Chien labrador = new Chien("Wof","yellow");
		Chien beagle = new Chien("Waf","black and white");
		System.out.println("The labrador says " + labrador.noise + " and its color is " + labrador.color);
		System.out.println("The beagle says " + beagle.noise + " and its color is " + beagle.color);
		System.out.println(String.format("The labrador says %s and its color is %s", labrador.noise, labrador.color));
		System.out.println(String.format("The beagle says %s and its color is %s", beagle.noise, beagle.color));
	}
}
